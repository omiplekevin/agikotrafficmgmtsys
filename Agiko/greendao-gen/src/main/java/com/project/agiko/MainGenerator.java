package com.project.agiko;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;

public class MainGenerator {

    private static final String PROJECT_DIR = System.getProperty("user.dir");

    public static void main(String[] args) {
        Schema schema = new Schema(1, "com.project.agiko.db");
        schema.enableKeepSectionsByDefault();

        addTables(schema);

        try {
            new DaoGenerator().generateAll(schema, PROJECT_DIR + "\\app\\src\\main\\java");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void addTables(final Schema schema) {
        Entity user = addUser(schema);
        Entity repo = addReport(schema);

        Property userId = repo.addLongProperty("userId").notNull().getProperty();
        user.addToMany(repo, userId, "userRepos");
    }

    private static Entity addUser(final Schema schema) {
        Entity user = schema.addEntity("User");
        user.addIdProperty().primaryKey().autoincrement();
        user.addStringProperty("firstname");
        user.addStringProperty("lastname");
        user.addStringProperty("user_type").notNull();
        user.addStringProperty("phone_number").notNull();
        user.addStringProperty("id_number");
        user.addStringProperty("password").notNull();

        return user;
    }

    private static Entity addReport(final Schema schema) {
        Entity report = schema.addEntity("Report");
        report.addIdProperty().primaryKey().autoincrement();
        report.addStringProperty("title").notNull();
        report.addLongProperty("timestamp").notNull();
        report.addStringProperty("description");
        report.addStringProperty("content_payload");
        report.addStringProperty("imageUrl");
        report.addStringProperty("owner");
        report.addStringProperty("owner_type");
        report.addStringProperty("type");
        // TODO: 5/6/2017 add image

        return report;
    }
}
