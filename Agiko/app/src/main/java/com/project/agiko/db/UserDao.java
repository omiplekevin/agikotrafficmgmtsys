package com.project.agiko.db;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.Property;
import de.greenrobot.dao.internal.DaoConfig;

import com.project.agiko.db.User;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table "USER".
*/
public class UserDao extends AbstractDao<User, Long> {

    public static final String TABLENAME = "USER";

    /**
     * Properties of entity User.<br/>
     * Can be used for QueryBuilder and for referencing column names.
    */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "_id");
        public final static Property Firstname = new Property(1, String.class, "firstname", false, "FIRSTNAME");
        public final static Property Lastname = new Property(2, String.class, "lastname", false, "LASTNAME");
        public final static Property User_type = new Property(3, String.class, "user_type", false, "USER_TYPE");
        public final static Property Phone_number = new Property(4, String.class, "phone_number", false, "PHONE_NUMBER");
        public final static Property Id_number = new Property(5, String.class, "id_number", false, "ID_NUMBER");
        public final static Property Password = new Property(6, String.class, "password", false, "PASSWORD");
    };

    private DaoSession daoSession;


    public UserDao(DaoConfig config) {
        super(config);
    }
    
    public UserDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
        this.daoSession = daoSession;
    }

    /** Creates the underlying database table. */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"USER\" (" + //
                "\"_id\" INTEGER PRIMARY KEY AUTOINCREMENT ," + // 0: id
                "\"FIRSTNAME\" TEXT," + // 1: firstname
                "\"LASTNAME\" TEXT," + // 2: lastname
                "\"USER_TYPE\" TEXT NOT NULL ," + // 3: user_type
                "\"PHONE_NUMBER\" TEXT NOT NULL ," + // 4: phone_number
                "\"ID_NUMBER\" TEXT," + // 5: id_number
                "\"PASSWORD\" TEXT NOT NULL );"); // 6: password
    }

    /** Drops the underlying database table. */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"USER\"";
        db.execSQL(sql);
    }

    /** @inheritdoc */
    @Override
    protected void bindValues(SQLiteStatement stmt, User entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        String firstname = entity.getFirstname();
        if (firstname != null) {
            stmt.bindString(2, firstname);
        }
 
        String lastname = entity.getLastname();
        if (lastname != null) {
            stmt.bindString(3, lastname);
        }
        stmt.bindString(4, entity.getUser_type());
        stmt.bindString(5, entity.getPhone_number());
 
        String id_number = entity.getId_number();
        if (id_number != null) {
            stmt.bindString(6, id_number);
        }
        stmt.bindString(7, entity.getPassword());
    }

    @Override
    protected void attachEntity(User entity) {
        super.attachEntity(entity);
        entity.__setDaoSession(daoSession);
    }

    /** @inheritdoc */
    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    /** @inheritdoc */
    @Override
    public User readEntity(Cursor cursor, int offset) {
        User entity = new User( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
            cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1), // firstname
            cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2), // lastname
            cursor.getString(offset + 3), // user_type
            cursor.getString(offset + 4), // phone_number
            cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5), // id_number
            cursor.getString(offset + 6) // password
        );
        return entity;
    }
     
    /** @inheritdoc */
    @Override
    public void readEntity(Cursor cursor, User entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setFirstname(cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1));
        entity.setLastname(cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2));
        entity.setUser_type(cursor.getString(offset + 3));
        entity.setPhone_number(cursor.getString(offset + 4));
        entity.setId_number(cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5));
        entity.setPassword(cursor.getString(offset + 6));
     }
    
    /** @inheritdoc */
    @Override
    protected Long updateKeyAfterInsert(User entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    /** @inheritdoc */
    @Override
    public Long getKey(User entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    /** @inheritdoc */
    @Override    
    protected boolean isEntityUpdateable() {
        return true;
    }
    
}
