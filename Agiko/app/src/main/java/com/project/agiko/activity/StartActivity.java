package com.project.agiko.activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.NumberPicker;

import com.project.agiko.Constants;
import com.project.agiko.R;

public class StartActivity extends AppCompatActivity {

    private static final int PERMISSION_REQUESTS = 1001;
    private static final String LOCATION_PERMISSION = "location_permission";
    private static final String LOCATION_GPS = "location_gps";
    private static final String[] REQUIRED_PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.CAMERA};

    private FrameLayout llSecondary;
    private NumberPicker countryPicker;

    private SharedPreferences prefs;
    private SharedPreferences.Editor edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        prefs = getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        edit = prefs.edit();

        if (prefs.getBoolean(Constants.LOGGED_IN, false)) {
            Constants.USER_ID = prefs.getLong(Constants.USER_ID_PREFS, -1L);
            Intent intent = new Intent(StartActivity.this, AgikoNavigationActivity.class);
            startActivity(intent);
            finish();
        }

        llSecondary = (FrameLayout) findViewById(R.id.llSecondary);
        countryPicker = (NumberPicker) findViewById(R.id.countryPicker);

        llSecondary.setVisibility(View.GONE);

        countryPicker.setMaxValue(getResources().getStringArray(R.array.country_list).length);
        countryPicker.setMinValue(0);
        countryPicker.setWrapSelectorWheel(false);
        countryPicker.setDisplayedValues(getResources().getStringArray(R.array.country_list));
        countryPicker.setValue(171); //setting default value to PH

        findViewById(R.id.btnSelect).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edit.putString("country", getResources().getStringArray(R.array.country_list)[countryPicker.getValue()]);
                edit.apply();

                Intent intent = new Intent(StartActivity.this, WelcomeActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkPermission();
    }

    private void checkPermission() {
        if (!hasCompletePermissions(REQUIRED_PERMISSIONS)) {
            Log.d(getClass().getSimpleName(), "requesting permission...");
            // Check Permissions Now
            ActivityCompat.requestPermissions(this, REQUIRED_PERMISSIONS, PERMISSION_REQUESTS);
        } else {
            Log.d(getClass().getSimpleName(), "initializing Google Maps...");
            if (!checkGPSAvailability()) {
                showSettingsAlert(LOCATION_GPS);
            } else {
                if (prefs.getString("country", "").equals("")) {
                    //no country detail yet - select country
                    llSecondary.setVisibility(View.VISIBLE);
                } else {
                    //there's already a country detail - proceed to welcome activity
                    llSecondary.setVisibility(View.GONE);
                    Intent intent = new Intent(StartActivity.this, WelcomeActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        }
    }

    private boolean hasCompletePermissions(String[] permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && permissions != null) {
            for (String permission : permissions) {
                Log.d(getClass().getSimpleName(), "checking permission: " + permission + " = " + ActivityCompat.checkSelfPermission(this, permission));
                if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean checkGPSAvailability() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            return false;
        }
        return true;
    }

    public void showSettingsAlert(final String permissionType) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        String t = "";
        if (permissionType.equals(LOCATION_GPS)) {
            t = "not turned on";
        } else {
            t = "not permitted";
        }
        alertDialog.setTitle("GPS settings");
        alertDialog.setMessage("GPS is " + t + ". This application requires your GPS provider to be permitted and turned on.");
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (permissionType.equals(LOCATION_GPS)) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                } else if (permissionType.equals(LOCATION_PERMISSION)) {
                    Intent intent = new Intent();
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                    intent.setData(uri);
                    startActivity(intent);
                }
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                System.exit(0);
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }
}
