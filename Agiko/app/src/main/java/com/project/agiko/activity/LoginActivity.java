package com.project.agiko.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.project.agiko.Agiko;
import com.project.agiko.Constants;
import com.project.agiko.R;
import com.project.agiko.db.DaoSession;
import com.project.agiko.db.User;
import com.project.agiko.db.UserDao;

import java.util.List;

public class LoginActivity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener {

    private final String TAG = getClass().getSimpleName();

    private RadioGroup radioGroup;
    private RadioButton rbOfficer, rbDriver;
    private LinearLayout pNumberLayout, idNumberLayout;
    private TextView instruction;
    private EditText password, phoneNumber, idnumber;
    private ImageView btnBack;
    private String type;

    private DaoSession daoSession;
    private UserDao userDao;
    private List<User> userList;
/*
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor edit;*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

//        sharedPreferences = getPreferences(Context.MODE_PRIVATE);
        daoSession = ((Agiko) getApplication()).getDaoSession();
        userDao = daoSession.getUserDao();
        userList = userDao.queryBuilder().list();

        initialize();
    }

    private void initialize() {
        pNumberLayout = (LinearLayout) findViewById(R.id.pNumberLayout);
        idNumberLayout = (LinearLayout) findViewById(R.id.idNumberLayout);
        instruction = (TextView) findViewById(R.id.instruction);
        btnBack = (ImageView) findViewById(R.id.btnBack);
        password = (EditText) findViewById(R.id.password);
        idnumber = (EditText) findViewById(R.id.idNumber);
        phoneNumber = (EditText) findViewById(R.id.phoneNumber);

        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        rbOfficer = (RadioButton) findViewById(R.id.rbOfficer);
        rbDriver = (RadioButton) findViewById(R.id.rbDriver);
        radioGroup.setOnCheckedChangeListener(this);
        radioGroup.check(rbOfficer.getId());

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, WelcomeActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
        if (rbOfficer.isChecked()) {
            pNumberLayout.setVisibility(View.GONE);
            idNumberLayout.setVisibility(View.VISIBLE);
            instruction.setText(R.string.enter_idnumber);
            type = Constants.USER_OFFICER;
        } else if (rbDriver.isChecked()) {
            pNumberLayout.setVisibility(View.VISIBLE);
            idNumberLayout.setVisibility(View.GONE);
            instruction.setText(R.string.enter_mobile);
            type = Constants.USER_DRIVER;
        }
    }

    public void signin(View v) {
        //lets check for empty fields
        if (type.equals(Constants.USER_DRIVER) && (password.getText().toString().equals("") || phoneNumber.getText().toString().equals(""))) {
            //DRIVER
            Toast.makeText(this, "Please fill in all required field.", Toast.LENGTH_SHORT).show();
        } else if (type.equals(Constants.USER_OFFICER) && (password.getText().toString().equals("") || idnumber.getText().toString().equals(""))){
            //OFFICER
            Toast.makeText(this, "Please fill in all required field.", Toast.LENGTH_SHORT).show();
        } else {
            Constants.USER_ID = getUserId(type);
            if (Constants.USER_ID == -1) {
                Toast.makeText(this,
                        "Your " + (type.equals(Constants.USER_DRIVER) ? "phone number" : "id number") + " or password is incorrect.",
                        Toast.LENGTH_SHORT)
                        .show();
            } else {
                SharedPreferences prefs = getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor prefsEditor = prefs.edit();
                prefsEditor.putBoolean(Constants.LOGGED_IN, true);
                prefsEditor.putLong(Constants.USER_ID_PREFS, Constants.USER_ID);
                prefsEditor.apply();
                Intent intent = new Intent(LoginActivity.this, AgikoNavigationActivity.class);
                startActivity(intent);
                finish();
            }
        }
    }

    private long getUserId(String type) {
        for (int i = 0; i < userList.size(); i++) {
            if (type.equals(Constants.USER_DRIVER)) {
                if (userList.get(i).getPhone_number().equals(phoneNumber.getText().toString()) &&
                        userList.get(i).getPassword().equals(password.getText().toString())) {
                    return userList.get(i).getId();
                }
            } else {
                if (userList.get(i).getId_number().equals(idnumber.getText().toString()) &&
                        userList.get(i).getPassword().equals(password.getText().toString())) {
                    return userList.get(i).getId();
                }
            }
        }

        return -1L;
    }
}
