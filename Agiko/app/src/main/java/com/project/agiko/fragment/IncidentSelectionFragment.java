package com.project.agiko.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.project.agiko.Constants;
import com.project.agiko.R;
import com.project.agiko.activity.AgikoNavigationActivity;

/**
 * Created by OMIPLEKEVIN on 017 Apr 17, 2017.
 */

public class IncidentSelectionFragment extends Fragment implements View.OnClickListener{

    private ImageButton btnOfficerReport;
    private ImageButton btnTrafficJamReport;
    private ImageButton btnAccidentReport;
    private ImageButton btnRoadConstReport;
    private ImageButton btnCalamityReport;

    private LinearLayout wildCardTile;
    private View space;

    private String address = "";
    private String latlng = "";

    public static IncidentSelectionFragment newInstance(String latlng, String address) {
        IncidentSelectionFragment frag = new IncidentSelectionFragment();
        Bundle args = new Bundle();
        args.putString("address", address);
        args.putString("latlng", latlng);
        frag.setArguments(args);
        return frag;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        address = getArguments().getString("address");
        latlng = getArguments().getString("latlng");
        View v = inflater.inflate(R.layout.fragment_road_report_selection, container, false);
        btnOfficerReport = (ImageButton) v.findViewById(R.id.btnOfficerReport);
        btnOfficerReport.setOnClickListener(this);
        btnTrafficJamReport = (ImageButton) v.findViewById(R.id.btnTrafficJamReport);
        btnTrafficJamReport.setOnClickListener(this);
        btnAccidentReport = (ImageButton) v.findViewById(R.id.btnAccidentReport);
        btnAccidentReport.setOnClickListener(this);
        btnRoadConstReport = (ImageButton) v.findViewById(R.id.btnRoadConstReport);
        btnRoadConstReport.setOnClickListener(this);
        btnCalamityReport = (ImageButton) v.findViewById(R.id.btnCalamityReport);
        btnCalamityReport.setOnClickListener(this);

        wildCardTile = (LinearLayout) v.findViewById(R.id.wildCardTile);
        space = v.findViewById(R.id.space);

        if (Constants.USER_TYPE.endsWith(Constants.USER_OFFICER)) {
            wildCardTile.setVisibility(View.VISIBLE);
            space.setVisibility(View.GONE);
        } else {
            wildCardTile.setVisibility(View.GONE);
            space.setVisibility(View.VISIBLE);
        }

        return v;
    }

    @Override
    public void onClick(View view) {
        String type = "";
        switch (view.getId()) {
            case R.id.btnOfficerReport:
                type = getActivity().getResources().getString(R.string.incidentTypeOfficerReport);
                break;
            case R.id.btnTrafficJamReport:
                type = getActivity().getResources().getString(R.string.incidentTypeTrafficJamReport);
                break;
            case R.id.btnAccidentReport:
                type = getActivity().getResources().getString(R.string.incidentTypeAccidentReport);
                break;
            case R.id.btnRoadConstReport:
                type = getActivity().getResources().getString(R.string.incidentTypeRoadConstReport);
                break;
            case R.id.btnCalamityReport:
                type = getActivity().getResources().getString(R.string.incidentTypeCalamityReport);
                break;
        }
        ((AgikoNavigationActivity)getActivity()).createIncidentEntryFormOverlay(type, latlng, address);
    }
}
