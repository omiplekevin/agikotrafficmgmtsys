package com.project.agiko;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by OMIPLEKEVIN on 029 Jan 29, 2017.
 */

public class Utilities {
    /**
     * Request Configuration from <i>configuration source</i>
     *
     * @param sourceUrl         source url where we can get our configuration
     * @param method            POST/GET
     * @param map               Parameters for the POST method
     * @return <b>responseString</b> result of request
     */
    public static String sendHttpRequest(String sourceUrl, String method, TreeMap<String, String> map) {
        Log.d("Utilities", sourceUrl + "\n" + method + "\n" + map);
        StringBuilder responseString = new StringBuilder();
        try {
            //throws MalformedURLException
            URL urlSource = new URL(sourceUrl);
            //throws IOException
            HttpURLConnection httpUrlConnection = (HttpURLConnection) urlSource.openConnection();
            httpUrlConnection.setRequestMethod(method);
            httpUrlConnection.setConnectTimeout(10000);

            if (method.equalsIgnoreCase("POST")) {
                StringBuilder params = new StringBuilder();
                for (Map.Entry<String, String> entry : map.entrySet()) {
                    params.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
                }
                httpUrlConnection.setDoOutput(true);
                httpUrlConnection.setInstanceFollowRedirects(false);
                httpUrlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                httpUrlConnection.setRequestProperty("charset", "utf-8");
                OutputStreamWriter writer = new OutputStreamWriter(httpUrlConnection.getOutputStream());
                writer.write(params.substring(0, params.length() - 1));
                writer.flush();
            }

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpUrlConnection.getInputStream()));
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                responseString.append(line);
            }

            httpUrlConnection.getInputStream().close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return responseString.toString();
    }
}
