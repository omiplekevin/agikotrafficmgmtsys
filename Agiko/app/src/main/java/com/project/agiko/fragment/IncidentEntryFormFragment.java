package com.project.agiko.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.project.agiko.Agiko;
import com.project.agiko.Constants;
import com.project.agiko.R;
import com.project.agiko.Utilities;
import com.project.agiko.activity.AgikoNavigationActivity;
import com.project.agiko.db.DaoSession;
import com.project.agiko.db.ReportModel;
import com.project.agiko.db.User;
import com.project.agiko.db.UserDao;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.TreeMap;

/**
 * Created by OMIPLEKEVIN on 024 Apr 24, 2017.
 */

public class IncidentEntryFormFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "IEFF";
    public static final String EXTRA_ADDRESS = "address";
    public static final String EXTRA_INCIDENT = "incidentCategory";
    public static final String EXTRA_INCIDENT_LATLNG = "latlng";

    private static final String[] trafficSeverity = new String[]{"Moderate", "Heavy", "Stand Still"};
    private static final String[] accidentSeverity = new String[]{"Minor", "Major", "Other Side"};
    private static final String[] calamitySeverity = new String[]{"Flood", "Landslide", "Falling Debris"};
    private static final int PICK_PHOTO = 1000;
    private static final int TAKE_PHOTO = 1001;

    private Cloudinary cloudinary;

    private String[] selectedSeverity;
    private String incidentType;
    private String incidentAddress;
    private String incidentLatLng;
    private String imageUploadedUrl;
    private String commentContent;
    private String tempCommentContent;
    private int selectedSeverityIndex = -1;

    private TextView incidentCategoryText;
    private TextView incidentPlaceText;
    private LinearLayout notice;
    private LinearLayout severityLinearHolder;
    private ImageView btnTrafficJamReportMain;
    private ImageButton selectGalleryPhotoImgBtn;
    private Button addCommentBtn;
    private Button sendBtn;

    public static IncidentEntryFormFragment newInstance(String incident, String latlng, String address) {
        IncidentEntryFormFragment frag = new IncidentEntryFormFragment();
        Bundle args = new Bundle();
        args.putString(EXTRA_ADDRESS, address);
        args.putString(EXTRA_INCIDENT, incident);
        args.putString(EXTRA_INCIDENT_LATLNG, latlng);
        frag.setArguments(args);
        return frag;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        incidentType = getArguments().getString(EXTRA_INCIDENT);
        incidentAddress = getArguments().getString(EXTRA_ADDRESS);
        incidentLatLng = getArguments().getString(EXTRA_INCIDENT_LATLNG);

        Log.d(getClass().getSimpleName(), "INCIDENT: " + incidentType);
        Log.d(getClass().getSimpleName(), "INCIDENT ADDRESS: " + incidentAddress);

        View view = inflater.inflate(R.layout.fragment_incident_entry_form, container, false);
        incidentCategoryText = (TextView) view.findViewById(R.id.incidentCategoryText);
        incidentPlaceText = (TextView) view.findViewById(R.id.incidentPlaceText);
        notice = (LinearLayout) view.findViewById(R.id.notice);
        severityLinearHolder = (LinearLayout) view.findViewById(R.id.severityLinearHolder);
        btnTrafficJamReportMain = (ImageView) view.findViewById(R.id.btnTrafficJamReportMain);
        selectGalleryPhotoImgBtn = (ImageButton) view.findViewById(R.id.selectGalleryPhotoImgBtn);
        selectGalleryPhotoImgBtn.setOnClickListener(this);
        addCommentBtn = (Button) view.findViewById(R.id.addCommentBtn);
        addCommentBtn.setOnClickListener(this);
        sendBtn = (Button) view.findViewById(R.id.sendBtn);
        sendBtn.setOnClickListener(this);

        incidentCategoryText.setText(getLabel(incidentType));
        incidentPlaceText.setText(incidentAddress);
        setIcon(incidentType);
        cloudinary = ((Agiko) getActivity().getApplication()).getCloudinary();

        getSeverityList(incidentType);
        if (selectedSeverity != null) {
            setupSeverityItems(severityLinearHolder);
        }

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sendBtn:
                //send POST REQUEST here...
                String type = "";
                if (selectedSeverityIndex == -1) {
                    type = "Road Construction";
                } else {
                    type = selectedSeverity[selectedSeverityIndex];
                }
                sendServerPostNotification(type, incidentLatLng, incidentType, incidentAddress, commentContent);
                break;
            case R.id.selectGalleryPhotoImgBtn:
                //select method
                imageActionSelectionDialog();
                break;
            case R.id.addCommentBtn:
                addCommentDialog();
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        Log.d(getClass().getSimpleName(), "requestCode: " + requestCode + ", resultCode: " + resultCode);
        if (requestCode == PICK_PHOTO && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                Log.d(getClass().getSimpleName(), "no result URI");
                return;
            } else {
                Log.d(getClass().getSimpleName(), "selected data: " + data.getData());
            }
            //upload image to Cloudinary
            try {
                new UploadImageToCloudinary().execute(getContext().getContentResolver().openInputStream(data.getData()));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        } else if (requestCode == TAKE_PHOTO && resultCode == Activity.RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            imageBitmap.compress(Bitmap.CompressFormat.PNG, 0, byteArrayOutputStream);
            byte[] bitmapdata = byteArrayOutputStream.toByteArray();
            ByteArrayInputStream bs = new ByteArrayInputStream(bitmapdata);
            //upload image to Cloudinary
            new UploadImageToCloudinary().execute(bs);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private String getLabel(String incidentType) {
        Log.d(getClass().getSimpleName(), "getting label: " + incidentType);
        switch (incidentType) {
            case "TrafficJamReport":
                return "Traffic Jam";
            case "AccidentReport":
                return "Accident";
            case "RoadConstructionReport":
                return "Road Construction";
            case "CalamityReport":
                return "Calamity";
            case "OfficerReport":
                return "Officer Report";
        }
        return "";
    }

    private void setIcon(String incidentType) {
        int drawable = 0;
        switch (incidentType) {
            case "OfficerReport":
                drawable = R.drawable.ic_officer;
                break;
            case "TrafficJamReport":
                drawable = R.drawable.ic_traffic;
                break;
            case "AccidentReport":
                drawable = R.drawable.ic_accident;
                break;
            case "RoadConstructionReport":
                drawable = R.drawable.ic_construction;
                break;
            case "CalamityReport":
                drawable = R.drawable.ic_disaster;
                break;
        }
        btnTrafficJamReportMain.setImageDrawable(ContextCompat.getDrawable(getContext(), drawable));
    }

    private void getSeverityList(String incidentType) {
        switch (incidentType) {
            case "TrafficJamReport":
                selectedSeverity = trafficSeverity;
                break;
            case "AccidentReport":
                selectedSeverity = accidentSeverity;
                break;
            case "CalamityReport":
                selectedSeverity = calamitySeverity;
                break;
        }
    }

    private void setupSeverityItems(final LinearLayout parentLayout) {
        Log.d(getClass().getSimpleName(), "setting up setupSeverityItems");
        if (parentLayout != null) {
            parentLayout.removeAllViews();
            for (int i = 0; i < selectedSeverity.length; i++) {
                String severityItem = selectedSeverity[i];
                Log.d(getClass().getSimpleName(), "creating view " + severityItem);
                LinearLayout.LayoutParams childParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1F);
                View v = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_subview_severity, null);
                final Button severityIcon = (Button) v.findViewById(R.id.btnTrafficJamReport);
                setSeverityIcon(severityIcon, i);
                final int ndx = i;
                severityIcon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        selectedSeverityIndex = ndx;
                        unselectTabs(parentLayout);
                        severityIcon.setSelected(true);
                    }
                });
                TextView sevText = (TextView) v.findViewById(R.id.severityLabel);
                sevText.setText(severityItem);
                v.setLayoutParams(childParams);
                parentLayout.addView(v);
            }
        } else {
            Log.d(getClass().getSimpleName(), "parentLayout is null");
        }
    }

    private void unselectTabs(LinearLayout parentLayout) {
        for (int i = 0; i < parentLayout.getChildCount(); i++) {
            parentLayout.getChildAt(i).findViewById(R.id.btnTrafficJamReport).setSelected(false);
        }
    }

    private void sendServerPostNotification(final String type, final String latlng, final String report, final String address, final String comment) {
        new AsyncTask<Void, Void, String>() {
            ReportModel reportModel;
            @Override
            protected String doInBackground(Void... voids) {
                DaoSession daoSession = ((Agiko) getActivity().getApplication()).getDaoSession();
                UserDao userDao = daoSession.getUserDao();
                List<User> users = userDao.queryBuilder()
                        .where(UserDao.Properties.Id.eq(Constants.USER_ID))
                        .list();
                String userId = "NO_ID";
                String userType = "NO_USER_TYPE";
                String userPhone = "";
                if (users.size() > 0) {
                    userId = users.get(0).getId_number();
                    userType = users.get(0).getUser_type();
                    if (!userType.equals(Constants.USER_OFFICER)) {
                        userPhone = users.get(0).getPhone_number();
                    }
                }

                TreeMap<String, String> params = new TreeMap<>();
                reportModel = new ReportModel();
                reportModel.lat = Double.valueOf(latlng.split(",")[0]);
                reportModel.lng = Double.valueOf(latlng.split(",")[1]);
                reportModel.timestamp = System.currentTimeMillis();
                reportModel.type = type;
                reportModel.imageUrl = imageUploadedUrl;
                reportModel.comment = comment;
                reportModel.report = report;
                reportModel.owner_type = userType;
                String reportJson = (new Gson()).toJson(reportModel);
                Log.d(TAG, "JSON POST NOTIFICATION: " + reportJson);
                params.put("payload", reportJson);
                params.put("headings", getResources().getString(R.string.default_notification_headings));
                params.put("fromuser", (userType.equals(Constants.USER_OFFICER) ? userId : userPhone));
                String response = Utilities.sendHttpRequest(Constants.URL_PUSH_NOTIFICATION, "POST", params);
                Log.d(TAG, "response from POST REQUEST: " + response);
                return response;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                //reset imageUploadedUrl
                try {
                    JSONObject jsonResponse = new JSONObject(result);
                    int popCount = 0;
                    if (jsonResponse.getBoolean("result")) {
                        for (int i = 0; i < getFragmentManager().getBackStackEntryCount(); i++) {
                            getFragmentManager().popBackStack();
                            popCount++;
                        }
                        Log.w(TAG, "onPostExecute: popped " + popCount + " times...");
                        imageUploadedUrl = "";
                        AgikoNavigationActivity.NewPostObject newPost = new AgikoNavigationActivity.NewPostObject();
                        newPost.newPostLocation = new LatLng(reportModel.lat, reportModel.lng);
                        EventBus.getDefault().post(newPost);
                    } else {
                        Toast.makeText(IncidentEntryFormFragment.this.getActivity(), "Failed to post. Please retry request.", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(IncidentEntryFormFragment.this.getActivity(), "Server error! Please check your server and try again.", Toast.LENGTH_LONG).show();
                    AlertDialog.Builder build = new AlertDialog.Builder(getActivity());
                    build.setMessage("Unable to send notification to server: " + Constants.URL_PUSH_NOTIFICATION);
                    build.show();
                }
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }
        }.execute();
    }

    private void setSeverityIcon(Button severityIcon, int index) {
        int drawableID = 0;
        switch (selectedSeverity[index]) {
            case "Moderate":
                drawableID = R.drawable.moderate_selector;
                break;
            case "Heavy":
                drawableID = R.drawable.heavy_selector;
                break;
            case "Stand Still":
                drawableID = R.drawable.standstill_selector;
                break;
            case "Minor":
                drawableID = R.drawable.minor_selector;
                break;
            case "Major":
                drawableID = R.drawable.major_selector;
                break;
            case "Other Side":
                drawableID = R.drawable.otherside_selector;
                break;
            case "Flood":
                drawableID = R.drawable.flood_selector;
                break;
            case "Landslide":
                drawableID = R.drawable.landslide_selector;
                break;
            case "Falling Debris":
                drawableID = R.drawable.fallingdebris_selector;
                break;
        }

        severityIcon.setBackground(ContextCompat.getDrawable(getContext(), drawableID));
    }

    /**
     * decide whether to capture a photo or use an existing image from the gallery
     *
     * @param action id of the action <br/> 1 - capture a photo using the system camera<br/> 2 - use an existing image from the gallery
     */
    private void captureOrPickImage(int action) {
        if (action == 1) {
            //camera
            startTakePictureIntent();
        } else {
            //pick image from gallery
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("image/*");
            startActivityForResult(intent, PICK_PHOTO);
        }
    }

    private void startTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, TAKE_PHOTO);
        }
    }

    private void addCommentDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final EditText serverIP = new EditText(getActivity());
        builder.setView(serverIP);
        serverIP.setInputType(EditorInfo.TYPE_NUMBER_FLAG_DECIMAL);
        serverIP.setHint("What happened here?");
        serverIP.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                tempCommentContent = editable.toString();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.setTitle("Post a comment");
        dialog.setButton(Dialog.BUTTON_POSITIVE, "Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                commentContent = serverIP.getText().toString();
            }
        });
        dialog.setButton(Dialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        dialog.show();
    }

    private void imageActionSelectionDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LinearLayout parentLayout = new LinearLayout(getActivity());
        parentLayout.setOrientation(LinearLayout.VERTICAL);

        Button btnTakePictureBtn = new Button(getActivity());
        btnTakePictureBtn.setAllCaps(false);
        btnTakePictureBtn.setText("Take a photo");
        parentLayout.addView(btnTakePictureBtn);

        Button btnSelectFromGallery = new Button(getActivity());
        btnSelectFromGallery.setAllCaps(false);
        btnSelectFromGallery.setText("Select from Gallery");
        parentLayout.addView(btnSelectFromGallery);
        builder.setView(parentLayout);
        final AlertDialog dialog = builder.create();
        btnTakePictureBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                captureOrPickImage(1);
            }
        });
        btnSelectFromGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                captureOrPickImage(2);
            }
        });
        dialog.setTitle("Choose Source");
        dialog.show();
    }

    private class UploadImageToCloudinary extends AsyncTask<InputStream, Void, String> {
        @Override
        protected void onPreExecute() {
            sendBtn.setEnabled(false);
            notice.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(InputStream... io) {
            try {
                InputStream inputStream = io[0];
                String filename = "agiko-" + System.currentTimeMillis();
                cloudinary.uploader().upload(inputStream, ObjectUtils.asMap("public_id", filename));
                String url = cloudinary.url().generate(filename + ".jpg");
                Log.d("Cloudinary", url);
                return url;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            sendBtn.setEnabled(true);
            if (s != null) {
                Log.d(getClass().getSimpleName(), "Uploaded image: " + s);
                imageUploadedUrl = s;
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                btnTrafficJamReportMain.setLayoutParams(params);
                Picasso.with(getActivity()).load(s).resize(1024, 768).centerInside().into(btnTrafficJamReportMain);
                notice.setVisibility(View.GONE);
            } else {
                Toast.makeText(getActivity(), "Unable to upload image...", Toast.LENGTH_LONG).show();
            }
        }
    }
}
