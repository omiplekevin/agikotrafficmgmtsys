package com.project.agiko;

public class Constants {

    public static final int POLYLINE_STROKE_WIDTH_PX = 25;

    public static final int COLOR_GREEN_ARGB = 0xff74daae;

    public static final int COLOR_BLUE_ARGB = 0xff303f9f;

    public static final int COLOR_GRAY_ARGB = 0xffcccccc;

    public static long USER_ID = 0L;

    public static String USER_TYPE = "";

    public static String USER_DRIVER = "Ordinary User";

    public static String USER_OFFICER = "TMC Officer";

    public static String USER_ID_PREFS = "user_id";

    public static String USER_FNAME = "firstname";

    public static String USER_LNAME = "lastname";

    public static String PHONE_NUMBER = "phone_number";

    public static String PASSWORD = "password";

    public static String SERVER_IP = "";

    public static String URL_PUSH_NOTIFICATION = "http://IP_HERE/navigationhere/test_onesignal_navigationhere.php";

    public static String URL_GMAPS_DIRECTIONS_API = "http://maps.googleapis.com/maps/api/directions/json?alternatives=true&origin=%1$s&destination=%2$s&region=ph";

    public static String LOGGED_IN = "logged_in";

}
