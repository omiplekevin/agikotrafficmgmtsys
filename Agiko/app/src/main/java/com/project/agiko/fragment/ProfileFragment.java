package com.project.agiko.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.project.agiko.Agiko;
import com.project.agiko.Constants;
import com.project.agiko.R;
import com.project.agiko.activity.WelcomeActivity;
import com.project.agiko.db.DaoSession;
import com.project.agiko.db.User;
import com.project.agiko.db.UserDao;

import java.util.List;

public class ProfileFragment extends Fragment implements View.OnClickListener {

    private View view;
    private ImageView shutdown;
    private TextView logout, save;
    private LinearLayout idNumber;
    private EditText fname, lname, number, pass, idnum, type;
    private String fn, ln, num, pas, idn, typ;
    private boolean hasChanged = false;

    private DaoSession daoSession;
    private UserDao userDao;
    private List<User> user;

    private SharedPreferences prefs;
    private SharedPreferences.Editor edit;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_profile, container, false);

        prefs = getActivity().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);

        shutdown = (ImageView) view.findViewById(R.id.shutdown);
        logout = (TextView) view.findViewById(R.id.logout);
        save = (TextView) view.findViewById(R.id.save);
        fname = (EditText) view.findViewById(R.id.fname);
        lname = (EditText) view.findViewById(R.id.lname);
        number = (EditText) view.findViewById(R.id.number);
        pass = (EditText) view.findViewById(R.id.pass);
        idnum = (EditText) view.findViewById(R.id.idnumbervalue);
        type = (EditText) view.findViewById(R.id.type);
        idNumber = (LinearLayout) view.findViewById(R.id.idnumber);

        shutdown.setOnClickListener(this);
        logout.setOnClickListener(this);
        save.setOnClickListener(this);

        daoSession = ((Agiko) getActivity().getApplication()).getDaoSession();
        userDao = daoSession.getUserDao();

        user = userDao.queryBuilder()
                .where(UserDao.Properties.Id.eq(Constants.USER_ID))
                .list();

        if (user.size() > 0) {
            fn = user.get(0).getFirstname();
            ln = user.get(0).getLastname();
            num = user.get(0).getPhone_number();
            pas = user.get(0).getPassword();
            idn = user.get(0).getId_number();
            typ = user.get(0).getUser_type();

            if (!fn.equals("")) {
                fname.setText(fn);
            }
            if (!ln.equals("")) {
                lname.setText(ln);
            }
            if (!num.equals("")) {
                number.setText(num);
            }
            if (!pas.equals("")) {
                pass.setText(pas);
            }
            if (!idn.equals("")) {
                idnum.setText(idn);
            }
            if (!typ.equals("")) {
                type.setText(typ);
            }

            if (typ.equals(Constants.USER_OFFICER)) {
                idNumber.setVisibility(View.VISIBLE);
            } else {
                idNumber.setVisibility(View.GONE);
            }
        } else {
            Log.e("ProfileFragment", "user.size() : " + user.size());
        }

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.logout:
                edit = prefs.edit();
                edit.putBoolean(Constants.LOGGED_IN, false);
                edit.commit();

                Intent intent = new Intent(getActivity(), WelcomeActivity.class);
                startActivity(intent);
                getActivity().finish();
                break;
            case R.id.shutdown:
                getActivity().finishAffinity();
                break;
            case R.id.save:
                if (!fn.equals(fname.getText().toString())) {
                    hasChanged = true;
                }
                if (!ln.equals(lname.getText().toString())) {
                    hasChanged = true;
                }
                if (!num.equals(number.getText().toString())) {
                    hasChanged = true;
                }
                if (!pas.equals(pass.getText().toString())) {
                    hasChanged = true;
                }
                if (hasChanged) {
                    user.get(0).setFirstname(fname.getText().toString());
                    user.get(0).setLastname(lname.getText().toString());
                    user.get(0).setId_number(idnum.getText().toString());
                    user.get(0).setPhone_number(number.getText().toString());
                    user.get(0).setPassword(pass.getText().toString());
                    userDao.update(user.get(0));

                    Toast.makeText(getActivity(), "Changes saved successfully.", Toast.LENGTH_SHORT).show();
                    hasChanged = false;
                }
        }
    }
}