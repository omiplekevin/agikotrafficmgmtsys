package com.project.agiko.db;

import com.google.gson.annotations.SerializedName;

/**
 * Created by OMIPLEKEVIN on May 09, 0009.
 */

public class ReportModel {

    @SerializedName("lat")
    public double lat;

    @SerializedName("lng")
    public double lng;

    @SerializedName("timestamp")
    public double timestamp;

    @SerializedName("report")
    public String report;

    @SerializedName("imageUrl")
    public String imageUrl = "";

    @SerializedName("type")
    public String type = "";

    @SerializedName("comment")
    public String comment;

    @SerializedName("owner_type")
    public String owner_type;
}
