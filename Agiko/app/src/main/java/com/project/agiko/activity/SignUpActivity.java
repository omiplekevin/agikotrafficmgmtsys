package com.project.agiko.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.project.agiko.Agiko;
import com.project.agiko.Constants;
import com.project.agiko.R;
import com.project.agiko.db.DaoSession;
import com.project.agiko.db.User;
import com.project.agiko.db.UserDao;

public class SignUpActivity extends AppCompatActivity {

    private LinearLayout pNumberLayout, idNumberLayout, signupLayout, verificationLayout;
    private String userType;
    private TextView tmcText;
    private EditText phoneNumber, password, idNumber;

    private DaoSession daoSession;
    private UserDao userDao;
    private boolean hasBlank = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        initialize();
    }

    private void initialize() {
        pNumberLayout = (LinearLayout) findViewById(R.id.pNumberLayout);
        idNumberLayout = (LinearLayout) findViewById(R.id.idNumberLayout);
        signupLayout = (LinearLayout) findViewById(R.id.activity_sign_up);
        verificationLayout = (LinearLayout) findViewById(R.id.verification_layout);

        tmcText = (TextView) findViewById(R.id.tmcText);
        phoneNumber = (EditText) findViewById(R.id.phoneNumber);
        password = (EditText) findViewById(R.id.password);
        idNumber = (EditText) findViewById(R.id.idNumber);

        userType = Constants.USER_TYPE;
        if (userType.equals(Constants.USER_OFFICER)) {
            idNumberLayout.setVisibility(View.VISIBLE);
            tmcText.setText(R.string.officer);
        } else if (userType.equals(Constants.USER_DRIVER)) {
            idNumberLayout.setVisibility(View.GONE);
            tmcText.setText(R.string.driver);
        }
    }

    public void signup(View v) {
        if (userType.equals(Constants.USER_OFFICER)) {
            if (idNumber.getText().toString().equals("") || phoneNumber.getText().toString().equals("") || password.getText().toString().equals("")) {
                hasBlank = true;
            }
        } else {
            if (phoneNumber.getText().toString().equals("") || password.getText().toString().equals("")) {
                hasBlank = true;
            }
        }

        if (!hasBlank) {
            daoSession = ((Agiko) getApplication()).getDaoSession();
            userDao = daoSession.getUserDao();

            User user = new User();
            user.setFirstname("");
            user.setLastname("");
            user.setId_number(idNumber.getText().toString());
            user.setPhone_number(phoneNumber.getText().toString());
            user.setPassword(password.getText().toString());
            user.setUser_type(userType);
            userDao.insert(user);
            Constants.USER_ID = user.getId();

            verify();
            /*signupLayout.setVisibility(View.GONE);
            verificationLayout.setVisibility(View.VISIBLE);*/
        } else {
            Toast.makeText(this, "Please fill in all fields.", Toast.LENGTH_SHORT).show();
            hasBlank = false;
        }
    }

    public void cancelSignup(View v) {
        Intent intent = new Intent(SignUpActivity.this, WelcomeActivity.class);
        startActivity(intent);
        finish();
    }

    public void verify(/*View v*/) {
        SharedPreferences prefs = getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putBoolean(Constants.LOGGED_IN, true);
        prefsEditor.putLong(Constants.USER_ID_PREFS, Constants.USER_ID);
        prefsEditor.apply();
        Intent intent = new Intent(SignUpActivity.this, AgikoNavigationActivity.class);
        startActivity(intent);
        finish();
    }
}
