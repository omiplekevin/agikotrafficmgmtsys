package com.project.agiko.helper;

import android.content.Context;
import android.content.SharedPreferences;

import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;
import com.project.agiko.R;

/**
 * DEVELOPER:       OMIPLEKEVIN<br/>
 * LAST MODIFIED:   July 27, 2017<br/>
 * IN CLASS:        com.project.agiko.helper<br/>
 * <br/>
 * //todo insert definition here...
 * <br/>
 **/
public class OneSignalNotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {
    public static final String NOTIFICATION_ID = "notificationID";

    private Context context;
    private static final String TAG = "OSOpenHandler";

    public OneSignalNotificationOpenedHandler(Context context) {
        this.context = context;
    }

    @Override
    public void notificationOpened(OSNotificationOpenResult result) {
        SharedPreferences prefs = context.getSharedPreferences(context.getResources().getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        prefs.edit().putString(NOTIFICATION_ID, result.notification.payload.notificationID).apply();
    }
}
