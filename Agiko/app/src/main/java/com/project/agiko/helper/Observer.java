package com.project.agiko.helper;

/**
 * Created by OMIPLEKEVIN on May 08, 0008.
 */

public interface Observer {
    void update();
}
