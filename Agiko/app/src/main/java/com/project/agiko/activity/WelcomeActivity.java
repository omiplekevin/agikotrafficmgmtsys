package com.project.agiko.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.project.agiko.Constants;
import com.project.agiko.R;

public class WelcomeActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView mOfficer, mUser, mLogin;
    private SharedPreferences prefs;
    private SharedPreferences.Editor edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        prefs = getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        edit = prefs.edit();

        /*if (prefs.getBoolean(Constants.LOGGED_IN, false)) { //logged in
            Intent intent = new Intent(WelcomeActivity.this, AgikoNavigationActivity.class);
            startActivity(intent);
            finish();
        }*/

        initialize();
    }

    private void initialize() {
        mOfficer = (TextView) findViewById(R.id.btnOfficer);
        mUser = (TextView) findViewById(R.id.btnUser);
        mLogin = (TextView) findViewById(R.id.btnLogin);

        mOfficer.setOnClickListener(this);
        mUser.setOnClickListener(this);
        mLogin.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.btnOfficer:
                /*edit.putString(Constants.USER_TYPE, Constants.USER_OFFICER);
                edit.commit();*/
                Constants.USER_TYPE = Constants.USER_OFFICER;
                intent = new Intent(WelcomeActivity.this, SignUpActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.btnUser:
                /*edit.putString(Constants.USER_TYPE, Constants.USER_DRIVER);
                edit.commit();*/
                Constants.USER_TYPE = Constants.USER_DRIVER;
                intent = new Intent(WelcomeActivity.this, SignUpActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.btnLogin:
                intent = new Intent(WelcomeActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }
}
