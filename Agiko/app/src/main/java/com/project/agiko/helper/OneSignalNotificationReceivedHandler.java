package com.project.agiko.helper;

import android.content.Context;
import android.util.Log;

import com.onesignal.OSNotification;
import com.onesignal.OneSignal;
import com.project.agiko.Agiko;

import org.json.JSONException;
import org.json.JSONObject;


public class OneSignalNotificationReceivedHandler implements OneSignal.NotificationReceivedHandler {

    private Context context;
    public static final String TAG = "OSReceivedHandler";

    public OneSignalNotificationReceivedHandler(Context context) {
        this.context = context;
    }

    @Override
    public void notificationReceived(final OSNotification notification) {
        try {
            JSONObject data = notification.payload.additionalData;
            if (notification.isAppInFocus) {
                Log.d(TAG, "" + data.toString() + ", " + notification.toString());
                JSONObject jsonObject = data.getJSONObject("content");
                if (data.getString("type").equals("incident")) {
                    ContentHelper.getInstance().insertContent((Agiko)context,
                            jsonObject.toString(),
                            notification.payload.body,
                            jsonObject.getString("owner"),
                            notification.payload.title);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
