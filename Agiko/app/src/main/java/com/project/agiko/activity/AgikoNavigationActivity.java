package com.project.agiko.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.JointType;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.RoundCap;
import com.google.gson.Gson;
import com.google.maps.android.PolyUtil;
import com.google.maps.android.SphericalUtil;
import com.project.agiko.Agiko;
import com.project.agiko.Constants;
import com.project.agiko.R;
import com.project.agiko.Utilities;
import com.project.agiko.db.Report;
import com.project.agiko.db.ReportModel;
import com.project.agiko.fragment.IncidentEntryFormFragment;
import com.project.agiko.fragment.IncidentSelectionFragment;
import com.project.agiko.fragment.ProfileFragment;
import com.project.agiko.helper.ContentHelper;
import com.project.agiko.helper.Observer;
import com.project.agiko.helper.OneSignalNotificationOpenedHandler;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class AgikoNavigationActivity extends FragmentActivity implements
        GoogleApiClient.OnConnectionFailedListener,
        OnMapReadyCallback,
        GoogleMap.OnMarkerClickListener,
        GoogleApiClient.ConnectionCallbacks,
        View.OnClickListener,
        View.OnLongClickListener,
        LocationListener,
        GoogleMap.OnMapLongClickListener,
        GoogleMap.OnCameraMoveStartedListener,
        Observer {

    private static final int REQUEST_LOCATION_INTERVAL = 3 * 1000; // 3 seconds
    private static final int UI_ELEMENT_REFOCUS = 1;
    private static final int UI_ELEMENT_INCIDENT = 2;
    private static final float WORLD = 6;
    private static final float LANDMASS = 10;
    private static final float CITY = 12;
    private static final float STREETS = 14;
    private static final float BUILDINGS = 16;
    private static final int MAX_RAD_DIST = 1 * 1000;  //1 Kms.
    private static final int REQUEST_ACTION_FROM_LOCATION = 1010;
    private static final int REQUEST_ACTION_TO_LOCATION = 1011;
    private static final String SHARED_PREFS_LAST_KNOWN_LOCATION = "last_known_location";
    private static final String SHARED_PREFS_SERVER_IP = "shared_prefs";
    public static String NOTIFICATION_ID = "";

    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient = null;
    private List<Polyline> polylineList;
    private Location myLocation;
    private LocationRequest locationRequest;
    private Geocoder geoCoder;
    private Toast toast;
    private ContentHelper contentHelper;
    private Marker selectedMarker;

    private SupportMapFragment mapFragment;
    private FrameLayout baseFrame;
    private FrameLayout detailHeader;
    private FrameLayout routeListFrame;
    private FrameLayout customRouteSearchFrame;
    private LinearLayout routeFooter;
    private ImageButton postActionImageBtn;
    private ImageButton postRefocusLocation;
    private Button closeRoutingPaneBtn;
    private Button searchForRoutesBtn;
    private ImageView profile;
    private ImageView searchLocation;
    private ImageView markerImage;
    private TextView markerTitle;
    private TextView markerDescription;
    private TextView markerPostLabel;
    private TextView updatesTextView;
    private ListView routeListView;
    private ProgressBar routeRequestProgress;
    private EditText origin;
    private EditText destination;

    private Date lastUpdateTime;
    private List<Report> reportList;
    private List<Marker> markerList;
    private boolean isUserDraggingMap = false;
    private boolean requestingUpdates = false;

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(getClass().getSimpleName(), "onCreate ");
        EventBus.getDefault().register(this);
        sharedPreferences = getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        String notificationId = sharedPreferences.getString(OneSignalNotificationOpenedHandler.NOTIFICATION_ID, "");
        NOTIFICATION_ID = notificationId;
        Log.w(getClass().getSimpleName(), "onCreate: opening " + NOTIFICATION_ID);
        initialize();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(getClass().getSimpleName(), "onStart ");
        String lastKnownLocation = sharedPreferences.getString(SHARED_PREFS_LAST_KNOWN_LOCATION, "");
        editor = sharedPreferences.edit();
        editor.putBoolean(Constants.LOGGED_IN, true);
        editor.apply();

        if (!lastKnownLocation.isEmpty()) {
            myLocation = new Location("");
            myLocation.setLatitude(Double.valueOf(lastKnownLocation.split("\\,")[0]));
            myLocation.setLongitude(Double.valueOf(lastKnownLocation.split("\\,")[1]));
            Log.d(getClass().getSimpleName(), "onStart: lastKnownLocation - " + lastKnownLocation);
        } else {
            Log.d(getClass().getSimpleName(), "onStart: no lastKnownLocation");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(getClass().getSimpleName(), "onResume...");
        initializeGoogleMaps();
        String ipAddress = sharedPreferences.getString(SHARED_PREFS_SERVER_IP, "");
        if (ipAddress.isEmpty()) {
            createIPChangeDialog(ipAddress);
        } else {
            Constants.SERVER_IP = ipAddress;
            Constants.URL_PUSH_NOTIFICATION = Constants.URL_PUSH_NOTIFICATION.replace("IP_HERE", Constants.SERVER_IP);
            Log.d(getClass().getSimpleName(), "Server IP changed: " + Constants.URL_PUSH_NOTIFICATION);
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        requestingUpdates = false;
        if (myLocation != null) {
            saveLastLocationPosition(myLocation.getLatitude(), myLocation.getLongitude());
        }
        stopLocationUpdates();
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        this.contentHelper.removeObserver(this);
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(getClass().getSimpleName(), "requestCode: " + requestCode + ", resultCode: " + resultCode);
        if (requestCode == REQUEST_ACTION_FROM_LOCATION) {
            if (resultCode == Activity.RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                origin.setText(place.getAddress());
                origin.setTag(place.getLatLng());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.i(getClass().getSimpleName(), status.getStatusMessage());
            } else if (resultCode == Activity.RESULT_CANCELED) {
                // The user canceled the operation.
            }
        } else if (requestCode == REQUEST_ACTION_TO_LOCATION) {
            if (resultCode == Activity.RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                destination.setText(place.getAddress());
                destination.setTag(place.getLatLng());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.i(getClass().getSimpleName(), status.getStatusMessage());
            } else if (resultCode == Activity.RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    ///////////////////////////////////////////////////////////////////////////
    // MAPS
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        try {
            mMap.setMyLocationEnabled(true);
            mMap.setOnMapLongClickListener(this);
            mMap.setOnMarkerClickListener(this);
            mMap.setOnCameraMoveStartedListener(this);
            mMap.getUiSettings().setCompassEnabled(false);
            mMap.getUiSettings().setMapToolbarEnabled(false);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
            contentHelper = ((Agiko) getApplication()).getContentHelper();
            contentHelper.registerObserver(this);

            if (geoCoder != null) {
                String selectedPlace = sharedPreferences.getString("country", "Philippines");
                try {
                    List<Address> addresses = geoCoder.getFromLocationName(selectedPlace, 1);
                    Location loc = new Location("");
                    loc.setLatitude(addresses.get(0).getLatitude());
                    loc.setLongitude(addresses.get(0).getLongitude());
                    focusOnLocation(loc, 4, false);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            addMarkerFromDatabase();
        } catch (SecurityException e) {
            Log.e(getClass().getSimpleName(), "Location services is disabled...");
            Toast.makeText(this, "Location is disabled, please enable location on Settings > Location", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d(getClass().getSimpleName(), "onConnected...");
        try {
            if (!requestingUpdates) {
                requestingUpdates = true;
                startLocationUpdate();
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(getClass().getSimpleName(), "onConnectionSuspended...");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(getClass().getSimpleName(), "onConnectionFailed...");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.postActionImageBtn:
                //switch between user or TMC for different layouts
//                focusOnLocation(myLocation);
                List<Address> addressList = getAddressList(myLocation.getLatitude(), myLocation.getLongitude());
                if (addressList != null) {
                    Log.d(getClass().getSimpleName(), "address list[" + addressList.size() + "]: " + Arrays.deepToString(addressList.toArray()));
                    createIncidentOverlay(
                            String.valueOf(myLocation.getLatitude()) + "," + String.valueOf(myLocation.getLongitude())
                            , addressList.get(0));
                } else {
                    Toast.makeText(this, "Can't get location address", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.postRefocusLocation:
                focusOnLocation(myLocation, -1, true);
                showCustomMapElement(UI_ELEMENT_INCIDENT);
                break;
            case R.id.closeRoutingPane:
                isUserDraggingMap = false;
                clearPolylines();
                routeRequestProgress.setVisibility(View.VISIBLE);
                routeListView.setVisibility(View.GONE);
                routeFooter.setVisibility(View.GONE);
                focusOnLocation(myLocation, -1, true);
                break;
            case R.id.profile:
                ProfileFragment fragment = new ProfileFragment();
                FragmentManager fManager = getSupportFragmentManager();
                FragmentTransaction transaction = fManager.beginTransaction();
                transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left);
                transaction.replace(R.id.overlays, fragment);
                transaction.addToBackStack("profile");
                transaction.commit();
                break;
            case R.id.markerImage:
                Dialog dialog = new Dialog(this);
                dialog.setContentView(R.layout.imagedialog);

                ImageView preview = (ImageView) dialog.findViewById(R.id.imagePreview);
                String imageUrl = ((ReportModel) selectedMarker.getTag()).imageUrl;
                Log.d(getClass().getSimpleName(), "trying to load: " + imageUrl);
                if (!imageUrl.isEmpty()) {
                    Picasso.with(this).load(imageUrl)
                            .error(R.drawable.broken_file)
                            .into(preview);
                    dialog.show();
                } else {
                    Log.d(getClass().getSimpleName(), "onMarkerClick: imageUrl is empty!");
                }
                break;
            case R.id.searchLocation:
                isUserDraggingMap = true;
                setupCustomRouteView();
                break;
            case R.id.searchRoute:
                isUserDraggingMap = true;
                if (origin.getTag() != null && destination.getTag() != null) {
                    setupDrivingInformation((LatLng)origin.getTag(), (LatLng)destination.getTag());
                    origin.setTag(null);
                    destination.setTag(null);
                } else {
                    toast.setText("Origin and/or Destination is not set!");
                    toast.show();
                }
                break;
            case R.id.origin:
            case R.id.destination:
                runIntent(view.getId() == R.id.origin ? REQUEST_ACTION_FROM_LOCATION : REQUEST_ACTION_TO_LOCATION);
                break;
            case R.id.updatesTextView:
                createIPChangeDialog(Constants.SERVER_IP);
                break;
        }
    }

    @Override
    public boolean onLongClick(View view) {
        switch (view.getId()) {
            case R.id.postActionImageBtn:
                if (isUserDraggingMap) {
                    isUserDraggingMap = false;
                } else {
                    isUserDraggingMap = true;
                }
                Log.d(getClass().getSimpleName(), (isUserDraggingMap ? "GPS follow mode" : "GPS unfollow mode"));
                if (toast != null) {
                    toast.setText((!isUserDraggingMap ? "GPS follow mode" : "GPS unfollow mode"));
                    toast.show();
                }
                break;
            case R.id.postRefocusLocation:
                break;
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            if (!isUserDraggingMap) {
                focusOnLocation(location, -1, true);
                showCustomMapElement(UI_ELEMENT_INCIDENT);
            }
            myLocation = location;
            lastUpdateTime = new Date();
            if (markerList != null) {
                List<Marker> nearMarkers = checkMarkerNearLocation(markerList);
                if (nearMarkers.size() == 0) {
                    Log.d(getClass().getSimpleName(), "no near markers...");
                }
            }
        }
    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        createLongPressActionDialog(latLng);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        detailHeader.setVisibility(View.VISIBLE);
        String[] infoSplit = marker.getSnippet().split("#!#");
        String description = infoSplit[0];
        markerTitle.setText(getLabel(marker.getTitle()));
        markerDescription.setText(description);
        markerPostLabel.setText("Posted by " + (infoSplit[2].equals(Constants.USER_DRIVER) ? "User " : "Officer ID ") + infoSplit[1]);
        for (Marker mark : markerList) {
            String lhs = ((ReportModel) marker.getTag()).imageUrl;
            String rhs = ((ReportModel) mark.getTag()).imageUrl;
            if (lhs != null && rhs != null) {
                if (lhs.equals(rhs)) {
                    selectedMarker = mark;
                    break;
                }
            }
        }
        if (selectedMarker != null) {
            String imageUrl = ((ReportModel) selectedMarker.getTag()).imageUrl;
            Log.d(getClass().getSimpleName(), "trying to load: " + imageUrl);
            if (!imageUrl.isEmpty()) {
                Picasso.with(this).load(imageUrl)
                        .resize(600, 800)
                        .centerInside()
                        .error(R.drawable.broken_file)
                        .into(markerImage);
            } else {
                Log.d(getClass().getSimpleName(), "onMarkerClick: imageUrl is empty!");
                markerImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.broken_file));
            }
        }
        //add marker image
        return true;
    }

    @Override
    public void onCameraMoveStarted(int i) {
        if (i == REASON_GESTURE) {
            isUserDraggingMap = true;
            showCustomMapElement(UI_ELEMENT_REFOCUS);
            if (detailHeader != null) {
                detailHeader.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void update() {
        Log.d(getClass().getSimpleName(), "updating google map...");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mMap != null) {
                    //remove first all markers
                    mMap.clear();
                    //add items from DB to the map
                    reportList = contentHelper.getReportList(getApplication());
                    addMarkerFromDatabase();
                } else {
                    Log.w(getClass().getSimpleName(), "mMap is null! can't add items...");
                }
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(NewPostObject newPostObject) {
        Location location = new Location("");
        location.setLatitude(newPostObject.newPostLocation.latitude);
        location.setLongitude(newPostObject.newPostLocation.longitude);
        focusOnLocation(location, (int)BUILDINGS, false);
    }

    ///////////////////////////////////////////////////////////////////////////
    // METHODS
    ///////////////////////////////////////////////////////////////////////////
    private void initialize() {
        setContentView(R.layout.activity_main);
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
//        profileContainer = (FrameLayout) findViewById(R.id.overlays);
        baseFrame = (FrameLayout) findViewById(R.id.baseFrame);
        detailHeader = (FrameLayout) findViewById(R.id.detailHeader);
        routeFooter = (LinearLayout) findViewById(R.id.routeFooter);
        routeListView = (ListView) findViewById(R.id.routeListView);
        routeListFrame = (FrameLayout) findViewById(R.id.routeListFrame);
        customRouteSearchFrame = (FrameLayout) findViewById(R.id.customRouteSearchFrame);
        updatesTextView = (TextView) findViewById(R.id.updatesTextView);
        updatesTextView.setOnClickListener(this);
        origin = (EditText) findViewById(R.id.origin);
        origin.setOnClickListener(this);
        origin.setTag(null);
        destination = (EditText) findViewById(R.id.destination);
        destination.setOnClickListener(this);
        destination.setTag(null);
        searchForRoutesBtn = (Button) findViewById(R.id.searchRoute);
        searchForRoutesBtn.setOnClickListener(this);
        routeRequestProgress = (ProgressBar) findViewById(R.id.routeRequestProgress);
        closeRoutingPaneBtn = (Button) findViewById(R.id.closeRoutingPane);
        closeRoutingPaneBtn.setOnClickListener(this);
        profile = (ImageView) findViewById(R.id.profile);
        profile.setOnClickListener(this);
        searchLocation = (ImageView) findViewById(R.id.searchLocation);
        searchLocation.setOnClickListener(this);
        postActionImageBtn = (ImageButton) findViewById(R.id.postActionImageBtn);
        postActionImageBtn.setOnClickListener(this);
        postActionImageBtn.setOnLongClickListener(this);
        postRefocusLocation = (ImageButton) findViewById(R.id.postRefocusLocation);
        postRefocusLocation.setOnClickListener(this);

        markerImage = (ImageView) findViewById(R.id.markerImage);
        markerTitle = (TextView) findViewById(R.id.markerTitle);
        markerDescription = (TextView) findViewById(R.id.markerDescription);
        markerPostLabel = (TextView) findViewById(R.id.markerPostLabel);
        markerImage.setOnClickListener(this);

        geoCoder = new Geocoder(this, Locale.getDefault());
        toast = Toast.makeText(this, "", Toast.LENGTH_LONG);
        locationRequest = new LocationRequest()
                .setInterval(REQUEST_LOCATION_INTERVAL)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        polylineList = new ArrayList<>();
    }

    private void initializeGoogleMaps() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
        mapFragment.getMapAsync(this);
    }

    private void startLocationUpdate() {
        try {
            if (mGoogleApiClient != null &&
                    locationRequest != null) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, locationRequest, this);
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    private void stopLocationUpdates() {
        try {
            if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    private void addMarkerFromDatabase() {
        markerList = new ArrayList<>();
        if (reportList == null) {
            reportList = this.contentHelper.getReportList(getApplication());
        }
        for (Report reportItem : reportList) {
            try {
                String truePayload = (new JSONObject(reportItem.getContent_payload())).getString("content_payload");
                Log.d("AgikoNavigationActivity", "addMarkerFromDatabase content-payload: " + reportItem.getContent_payload());
                ReportModel reportModel = (new Gson()).fromJson(truePayload, ReportModel.class);
                // create marker
                MarkerOptions marker = new MarkerOptions().position(new LatLng(reportModel.lat, reportModel.lng));
                // Changing marker icon
                Bitmap bmp = getMarkerIcon(reportModel.owner_type, reportModel.type);
                marker.icon(BitmapDescriptorFactory.fromBitmap(bmp));
                marker.title(reportModel.report);
                marker.snippet(reportItem.getDescription() + "\n\n" + reportModel.comment + "\n" + "#!#" + reportItem.getOwner() + "#!#" + reportModel.owner_type);
                //add marker
                if (mMap != null) {
                    Marker mark = mMap.addMarker(marker);
                    mark.setTag(reportModel);
                    markerList.add(mark);
                } else {
                    Log.d("AgikoNavigationActivity", "addMarkerFromDatabase mMap is null!");
                }
            } catch (JSONException e) {
                e.printStackTrace();
                if (this.contentHelper.removeContent(getApplication(), reportItem.getId())) {
                    Log.w("AgikoNavigationActivity", "removed an item from database due to " + e.getMessage());
                }
            } catch (Exception e) {
                e.printStackTrace();
                if (this.contentHelper.removeContent(getApplication(), reportItem.getId())) {
                    Log.w("AgikoNavigationActivity", "removed an item from database due to " + e.getMessage());
                }
            }
        }
    }

    private void runIntent(int requestCode) {
        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                .setCountry("PH")
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_ADDRESS | AutocompleteFilter.TYPE_FILTER_ESTABLISHMENT)
                .build();
        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .setFilter(typeFilter)
                            .build(this);
            startActivityForResult(intent, requestCode);
        } catch (GooglePlayServicesRepairableException e) {
            // TODO: Handle the error.
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
            e.printStackTrace();
        }
    }

    private Bitmap getMarkerIcon(String ownerType, String type) {
        Bitmap bitmap;
        switch (type.replace(" ", "").toLowerCase(Locale.getDefault())) {
            case "moderate":
                bitmap = resizeMapIcons(ownerType, (ownerType.equals(Constants.USER_DRIVER) ? "ic_traffic_moderate" : "ic_tmc_traffic_moderate"));
                break;
            case "heavy":
                bitmap = resizeMapIcons(ownerType, (ownerType.equals(Constants.USER_DRIVER) ? "ic_traffic_heavy" : "ic_tmc_traffic_heavy"));
                break;
            case "standstill":
                bitmap = resizeMapIcons(ownerType, (ownerType.equals(Constants.USER_DRIVER) ? "ic_traffic_standstill" : "ic_tmc_traffic_standstill"));
                break;
            case "minor":
                bitmap = resizeMapIcons(ownerType, (ownerType.equals(Constants.USER_DRIVER) ? "ic_accident_minor" : "ic_tmc_accident_minor"));
                break;
            case "major":
                bitmap = resizeMapIcons(ownerType, (ownerType.equals(Constants.USER_DRIVER) ? "ic_accident_major" : "ic_tmc_accident_major"));
                break;
            case "otherside":
                bitmap = resizeMapIcons(ownerType, (ownerType.equals(Constants.USER_DRIVER) ? "ic_accident_otherside" : "ic_tmc_accident_otherside"));
                break;
            case "flood":
                bitmap = resizeMapIcons(ownerType, (ownerType.equals(Constants.USER_DRIVER) ? "ic_calamity_flood" : "ic_tmc_calamity_flood"));
                break;
            case "landslide":
                bitmap = resizeMapIcons(ownerType, (ownerType.equals(Constants.USER_DRIVER) ? "ic_calamity_landslide" : "ic_tmc_calamity_landslide"));
                break;
            case "fallingdebris":
                bitmap = resizeMapIcons(ownerType, (ownerType.equals(Constants.USER_DRIVER) ? "ic_calamity_fallingdebris" : "ic_tmc_calamity_fallingdebris"));
                break;
            default:
                bitmap = resizeMapIcons(ownerType, (ownerType.equals(Constants.USER_DRIVER) ? "ic_accident_minor" : "ic_tmc_accident_minor"));
        }
        return bitmap;
    }

    private Bitmap resizeMapIcons(String ownerType, String iconName) {
        Bitmap imageBitmap = BitmapFactory.decodeResource(getResources(), getResources().getIdentifier(iconName, "drawable", getPackageName()));
        Bitmap resizedBitmap = null;
        if (ownerType.equals(Constants.USER_DRIVER)) {
            resizedBitmap = Bitmap.createScaledBitmap(imageBitmap, 120, 150, false);
        } else if (ownerType.equals(Constants.USER_OFFICER)){
            resizedBitmap = Bitmap.createScaledBitmap(imageBitmap, 120, 120, false);
        }
        return resizedBitmap;
    }

    private void showCustomMapElement(int elementID) {
        switch (elementID) {
            case UI_ELEMENT_REFOCUS:
                if (postRefocusLocation != null) {
                    if (postRefocusLocation.getVisibility() != View.VISIBLE) {
                        postRefocusLocation.setVisibility(View.VISIBLE);
                    }
                }
                if (postActionImageBtn != null) {
                    if (postActionImageBtn.getVisibility() == View.VISIBLE) {
                        postActionImageBtn.setVisibility(View.INVISIBLE);
                    }
                }
                break;
            case UI_ELEMENT_INCIDENT:
                if (postActionImageBtn != null) {
                    if (postActionImageBtn.getVisibility() != View.VISIBLE) {
                        postActionImageBtn.setVisibility(View.VISIBLE);
                    }
                }
                if (postRefocusLocation != null) {
                    if (postRefocusLocation.getVisibility() == View.VISIBLE) {
                        postRefocusLocation.setVisibility(View.INVISIBLE);
                    }
                }
                break;
        }
    }

    private void focusOnLocation(Location location, int zoomLevel, boolean animate) {
        if (location != null) {
            showCustomMapElement(UI_ELEMENT_INCIDENT);
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 13));

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(location.getLatitude(), location.getLongitude()))    // Sets the center of the map to location user
                    .zoom((zoomLevel > -1 ? zoomLevel : 17))                                                               // Sets the zoom
                    .build();                                                               // Creates a CameraPosition from the builder
            if (animate) {
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            } else {
                mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        } else {
            Log.e(getClass().getSimpleName(), "location in null, no focused area...");
        }
    }

    private String getLabel(String incidentType) {
        Log.d(getClass().getSimpleName(), "getting label: " + incidentType);
        switch (incidentType) {
            case "TrafficJamReport":
                return "Traffic Jam";
            case "AccidentReport":
                return "Accident";
            case "RoadConstructionReport":
                return "Road Construction";
            case "CalamityReport":
                return "Calamity";
            case "OfficerReport":
                return "Officer Report";
        }
        return "";
    }

    private List<Marker> checkMarkerNearLocation(List<Marker> markerList) {
        List<Marker> nearMarkers = new ArrayList<>();
        //in meters
        if (markerList != null) {
            for (int i = 0; i < markerList.size(); i++) {
                LatLng fromLoc = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
                LatLng toLoc = new LatLng(markerList.get(i).getPosition().latitude, markerList.get(i).getPosition().longitude);
                double dist = SphericalUtil.computeDistanceBetween(fromLoc, toLoc);
                if (dist <= MAX_RAD_DIST) {
                    Log.d(getClass().getSimpleName(), "found near marker @ " + toLoc.latitude + "," + toLoc.longitude + " @ dist: " + dist);
                    nearMarkers.add(markerList.get(i));
                }
            }
        }
        return nearMarkers;
    }

    public void createIncidentEntryFormOverlay(String type, String latlng, String address) {
        Log.d(getClass().getSimpleName(), "creating incident entry form... " + address + ", " + type);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.overlays, IncidentEntryFormFragment.newInstance(type, latlng, address));
        transaction.addToBackStack("incident_entry_form");
        transaction.commit();
    }

    private void createIncidentOverlay(String latlng, Address address) {
        String addr = address.getAddressLine(0) + ", " +
                address.getAddressLine(1) + ", " +
                address.getAddressLine(2) + ", " +
                address.getAddressLine(3);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.overlays, IncidentSelectionFragment.newInstance(latlng, addr));
        transaction.addToBackStack("incident_report_form");
        transaction.commit();
    }

    private void createIPChangeDialog(final String ipAddress) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final EditText serverIP = new EditText(this);
        serverIP.setInputType(EditorInfo.TYPE_NUMBER_FLAG_DECIMAL);
        builder.setView(serverIP);
        AlertDialog dialog = builder.create();
        if (!ipAddress.isEmpty()) {
            serverIP.setText(ipAddress);
        } else {
            serverIP.setHint("i.e 10.0.0.2");
        }
        dialog.setTitle("Server IP");
        dialog.setButton(Dialog.BUTTON_POSITIVE, "Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String oldServerIp;
                if (ipAddress.isEmpty()) {
                    oldServerIp = "IP_HERE";
                } else {
                    oldServerIp = Constants.SERVER_IP;
                }
                Constants.SERVER_IP = serverIP.getText().toString();
                Constants.URL_PUSH_NOTIFICATION = Constants.URL_PUSH_NOTIFICATION.replace(oldServerIp, Constants.SERVER_IP);
                Log.d(getClass().getSimpleName(), "Server IP changed: " + Constants.URL_PUSH_NOTIFICATION);
                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.putString(SHARED_PREFS_SERVER_IP, Constants.SERVER_IP);
                edit.apply();
            }
        });
        dialog.setButton(Dialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(AgikoNavigationActivity.this, "You need to set SERVER IP", Toast.LENGTH_LONG).show();
                System.exit(0);
            }
        });
        dialog.show();
    }

    private List<Address> getAddressList(double lat, double lng) {
        if (geoCoder != null) {
            try {
                return geoCoder.getFromLocation(lat, lng, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            Log.d(getClass().getSimpleName(), "GeoCoder is null");
        }
        return null;
    }

    private void createLongPressActionDialog(final LatLng latlng) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LinearLayout parentLayout = new LinearLayout(this);
        parentLayout.setOrientation(LinearLayout.VERTICAL);

        Button btnCreateIncidentReport = new Button(this);
        btnCreateIncidentReport.setAllCaps(false);
        btnCreateIncidentReport.setText("Post a report");
        parentLayout.addView(btnCreateIncidentReport);

        Button btnDriveFromAndTo = new Button(this);
        btnDriveFromAndTo.setAllCaps(false);
        btnDriveFromAndTo.setText("Drive to here");
        parentLayout.addView(btnDriveFromAndTo);
        builder.setView(parentLayout);
        final AlertDialog dialog = builder.create();

        btnCreateIncidentReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                List<Address> addressList = getAddressList(latlng.latitude, latlng.longitude);
                if (addressList != null) {
                    createIncidentOverlay(
                            String.valueOf(latlng.latitude) + "," + String.valueOf(latlng.longitude)
                            , addressList.get(0));
                } else {
                    Toast.makeText(AgikoNavigationActivity.this, "Can't get location address", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnDriveFromAndTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                isUserDraggingMap = true;
                setupDrivingInformation(new LatLng(myLocation.getLatitude(), myLocation.getLongitude()), latlng);
            }
        });
        dialog.setTitle("Choose Source");
        dialog.show();
    }

    public void setupDrivingInformation(final LatLng originLatLng, final LatLng destinationLatLng) {
        //you can do request directions here:
        //https://developers.google.com/maps/documentation/directions/intro#RequestParameters
        //i.e: http://maps.googleapis.com/maps/api/directions/json?origin=7.072301,125.614603&destination=7.072421,125.610659
        if (destinationLatLng != null && myLocation != null) {
            customRouteSearchFrame.setVisibility(View.GONE);
            routeListFrame.setVisibility(View.VISIBLE);
            routeFooter.setVisibility(View.VISIBLE);
            /*centerLocationBounds(new ArrayList<LatLng>(){{
                add(new LatLng(myLocation.getLatitude(), myLocation.getLongitude()));
                add(destinationLatLng);
            }});*/
            new DirectionsAPICall().execute(originLatLng, destinationLatLng);
        } else {
            Toast.makeText(this, "Can't retrieve location information", Toast.LENGTH_LONG).show();
        }
    }

    public void setupCustomRouteView() {
        customRouteSearchFrame.setVisibility(View.VISIBLE);
        routeListFrame.setVisibility(View.GONE);
        routeFooter.setVisibility(View.VISIBLE);
    }

    private void parseAndDisplayToMapPolylines(List<LatLng> decodedPolylines, boolean isMain) {
        Polyline polyline = mMap.addPolyline(new PolylineOptions()
                .addAll(decodedPolylines));

        polyline.setEndCap(new RoundCap());
        polyline.setWidth(Constants.POLYLINE_STROKE_WIDTH_PX);
        polyline.setColor((isMain ? Constants.COLOR_BLUE_ARGB : Constants.COLOR_GRAY_ARGB));
        polyline.setJointType(JointType.ROUND);
        if (polylineList == null) {
            polylineList = new ArrayList<>();
        } else {
            polylineList.add(polyline);
        }
    }

    private void setupRouteListView(final LatLng origin, final LatLng destination, JSONArray routes) {
        if (routes != null) {
            RouteListViewAdapter routeListViewAdapter = new RouteListViewAdapter(this, routes);
            routeListView.setAdapter(routeListViewAdapter);
            if (polylineList != null) {
                clearPolylines();
                polylineList.clear();
            }
            for (int i = routes.length() - 1; i >= 0; i--) {
                try {
                    parseAndDisplayToMapPolylines(
                            PolyUtil.decode(
                                    routes.getJSONObject(i)
                                            .getJSONObject("overview_polyline")
                                            .getString("points")),
                            i == 0);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            centerLocationBounds(origin, destination, new ArrayList<LatLng>() {{
                add(origin);
                add(destination);
            }});
        } else {
            Toast.makeText(this, "Unable to load directions. Please try again.", Toast.LENGTH_LONG).show();
            routeRequestProgress.setVisibility(View.VISIBLE);
            routeListView.setVisibility(View.GONE);
        }
    }

    private void clearPolylines() {
        if (polylineList != null || polylineList.size() > 0) {
            for (Polyline polyline : polylineList) {
                polyline.remove();
            }
        }
    }

    private void saveLastLocationPosition(double lat, double lng) {
        editor.putString(SHARED_PREFS_LAST_KNOWN_LOCATION, lat + "," + lng);
        editor.commit();
    }

    private void centerLocationBounds(LatLng origin, LatLng destination, ArrayList<Marker> locations) {
        List<LatLng> locationList = new ArrayList<>();
        for (Marker location : locations) {
            locationList.add(location.getPosition());
        }
        centerLocationBounds(origin, destination, locationList);
    }

    private void centerLocationBounds(LatLng origin, LatLng destination, List<LatLng> locations) {
        if (locations != null || locations.size() > 0) {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (LatLng location : locations) {
                builder.include(location);
            }
            LatLngBounds bounds = builder.build();
            if (mMap != null) {
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(bounds.getCenter(), getZoomLevelFromDistance(origin, destination)));
            } else {
                Log.d(getClass().getSimpleName(), "centerLocationBounds: mMap is null!");
            }
        } else {
            Log.d(getClass().getSimpleName(), "centerLocationBounds: locations is null!");
        }
    }

    private float getZoomLevelFromDistance(LatLng origin, LatLng destination) {
        double dist = SphericalUtil.computeDistanceBetween(origin, destination);
        Log.d(getClass().getSimpleName(), "getZoomLevelFromDistance: dist - " + dist);
        if (dist < 700) {
            Log.d(getClass().getSimpleName(), "getZoomLevelFromDistance: BUILDINGS");
            return BUILDINGS;
        } else if (dist < 5000) {
            Log.d(getClass().getSimpleName(), "getZoomLevelFromDistance: STREETS");
            return STREETS;
        } else if (dist < 10000) {
            Log.d(getClass().getSimpleName(), "getZoomLevelFromDistance: CITY");
            return CITY;
        } else if (dist < 100000) {
            Log.d(getClass().getSimpleName(), "getZoomLevelFromDistance: LANDMASS");
            return LANDMASS;
        } else {
            Log.d(getClass().getSimpleName(), "getZoomLevelFromDistance: WORLD");
            return WORLD;
        }
    }

    private class DirectionsAPICall extends AsyncTask<LatLng, Void, JSONArray> {

        LatLng destination, origin;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            routeRequestProgress.setVisibility(View.VISIBLE);
        }

        @Override
        protected JSONArray doInBackground(LatLng... latlng) {
            try {
                origin = latlng[0];
                destination = latlng[1];
                String withInsertedLocations = String.format(Constants.URL_GMAPS_DIRECTIONS_API,
                        origin.latitude + "," + origin.longitude,
                        destination.latitude + "," + destination.longitude);
                Log.d("DIRECTIONS-API", withInsertedLocations);
                String jsonDirectionResponse = Utilities.sendHttpRequest(withInsertedLocations, "GET", null);
                JSONObject gMapsDirectionAPIResponse = new JSONObject(jsonDirectionResponse);
                return gMapsDirectionAPIResponse
                        .getJSONArray("routes");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONArray result) {
            super.onPostExecute(result);
            if (routeListView != null) {
                routeListView.setVisibility(View.VISIBLE);
                routeRequestProgress.setVisibility(View.GONE);
                setupRouteListView(origin, destination, result);
            }
//            if (!result.isEmpty()) {
//                parseAndDisplayToMapPolylines(PolyUtil.decode(result));
//            }
        }
    }

    private class RouteListViewAdapter extends BaseAdapter {

        Context context;
        JSONArray routeArray;

        public RouteListViewAdapter(Context context, JSONArray routes) {
            this.context = context;
            this.routeArray = routes;
        }

        @Override
        public int getCount() {
            return routeArray.length();
        }

        @Override
        public JSONObject getItem(int i) {
            try {
                return routeArray.getJSONObject(i);
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHolder viewHolder;
            if (view == null) {
                viewHolder = new ViewHolder();
                view = LayoutInflater.from(context).inflate(R.layout.listview_route_view_item, null);
                viewHolder.fromDestination = (TextView) view.findViewById(R.id.fromDestination);
                viewHolder.toDestination = (TextView) view.findViewById(R.id.toDestination);
                viewHolder.distance = (TextView) view.findViewById(R.id.distance);
                viewHolder.duration = (TextView) view.findViewById(R.id.duration);
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }

            try {
                viewHolder.fromDestination.setText(routeArray.getJSONObject(i).getJSONArray("legs").getJSONObject(0).getString("end_address"));
                viewHolder.toDestination.setText(routeArray.getJSONObject(i).getString("summary"));
                viewHolder.distance.setText(routeArray.getJSONObject(i).getJSONArray("legs").getJSONObject(0).getJSONObject("distance").getString("text"));
                viewHolder.duration.setText(routeArray.getJSONObject(i).getJSONArray("legs").getJSONObject(0).getJSONObject("duration").getString("text"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return view;
        }

        class ViewHolder {
            public TextView fromDestination;
            public TextView toDestination;
            public TextView distance;
            public TextView duration;
        }
    }

    public static class NewPostObject {
        public LatLng newPostLocation;
    }

}
