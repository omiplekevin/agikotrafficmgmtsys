package com.project.agiko.helper;

/**
 * Created by OMIPLEKEVIN on May 08, 0008.
 */

public interface Subject {
    void registerObserver(Observer o);
    void removeObserver(Observer o);
    void notifyObservers();
}
