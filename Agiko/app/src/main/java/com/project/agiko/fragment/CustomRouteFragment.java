package com.project.agiko.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.model.LatLng;
import com.project.agiko.R;
import com.project.agiko.activity.AgikoNavigationActivity;

/**
 * DEVELOPER:       OMIPLEKEVIN<br/>
 * LAST MODIFIED:   June 22, 2017<br/>
 * IN CLASS:        com.project.agiko.fragment<br/>
 * <br/>
 * //todo insert definition here...
 * <br/>
 **/
public class CustomRouteFragment extends Fragment implements View.OnFocusChangeListener{

    private View view;
    private EditText fromDestination;
    private EditText toDestination;
    private Button createRoute;

    private LatLng fromLatLng;
    private LatLng toLatLng;

    private static final int REQUEST_ACTION_FROM_LOCATION = 1010;
    private static final int REQUEST_ACTION_TO_LOCATION = 1011;

    public static CustomRouteFragment newInstance() {
        Bundle args = new Bundle();
        CustomRouteFragment fragment = new CustomRouteFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_custom_route, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (view != null) {
            fromDestination = (EditText) view.findViewById(R.id.fromDestination);
            fromDestination.setOnFocusChangeListener(this);
            toDestination = (EditText) view.findViewById(R.id.toDestination);
            toDestination.setOnFocusChangeListener(this);
            createRoute = (Button) view.findViewById(R.id.createRoute);
            createRoute.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((AgikoNavigationActivity)getActivity()).setupDrivingInformation(fromLatLng, toLatLng);
                    getFragmentManager().popBackStack();
                }
            });
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ACTION_FROM_LOCATION) {
            if (resultCode == Activity.RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(getActivity(), data);
                fromDestination.setText(place.getAddress());
                fromLatLng = place.getLatLng();
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getActivity(), data);
                // TODO: Handle the error.
                Log.i(getClass().getSimpleName(), status.getStatusMessage());
            } else if (resultCode == Activity.RESULT_CANCELED) {
                // The user canceled the operation.
            }
        } else if (requestCode == REQUEST_ACTION_TO_LOCATION) {
            if (resultCode == Activity.RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(getActivity(), data);
                toDestination.setText(place.getAddress());
                toLatLng = place.getLatLng();
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getActivity(), data);
                // TODO: Handle the error.
                Log.i(getClass().getSimpleName(), status.getStatusMessage());
            } else if (resultCode == Activity.RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        switch (view.getId()) {
            case R.id.fromDestination:
                runIntent(REQUEST_ACTION_FROM_LOCATION);
                break;
            case R.id.toDestination:
                runIntent(REQUEST_ACTION_TO_LOCATION);
                break;
            default:
                Log.d("CustomRouteFragment", "onFocusChange can't find id: " + view.getId());
        }
    }

    private void runIntent(int requestCode) {
        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                .setCountry("PH")
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_ADDRESS|AutocompleteFilter.TYPE_FILTER_ESTABLISHMENT)
                .build();
        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .setFilter(typeFilter)
                            .build(getActivity());
            startActivityForResult(intent, requestCode);
        } catch (GooglePlayServicesRepairableException e) {
            // TODO: Handle the error.
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
            e.printStackTrace();
        }
    }
}
