package com.project.agiko;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.cloudinary.Cloudinary;
import com.onesignal.OneSignal;
import com.project.agiko.db.DaoMaster;
import com.project.agiko.db.DaoSession;
import com.project.agiko.helper.ContentHelper;
import com.project.agiko.helper.OneSignalNotificationOpenedHandler;
import com.project.agiko.helper.OneSignalNotificationReceivedHandler;

import java.util.HashMap;
import java.util.Map;

public class Agiko extends Application {

    private ContentHelper contentHelper;
    private Cloudinary cloudinary;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(getClass().getSimpleName(), "onCreate Agiko Application starts... initializing OneSignal...");
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .setNotificationReceivedHandler(new OneSignalNotificationReceivedHandler(this))
                .setNotificationOpenedHandler(new OneSignalNotificationOpenedHandler(this))
                .init();

        //instantiate ContentHelper
        contentHelper = ContentHelper.getInstance();
    }



    public DaoSession getDaoSession() {
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, BuildConfig.databaseSchema, null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        return daoMaster.newSession();
    }

    public Cloudinary getCloudinary() {
        if (cloudinary == null) {
            Map<String, String> config = new HashMap<>();
            config.put("cloud_name", "horbordidorp");
            config.put("api_key", "872388742584913");
            config.put("api_secret", "hKJRyPNQUS7JnHDJ1TlAQzLFN7E");
            cloudinary = new Cloudinary(config);
        }
        return cloudinary;
    }

    public ContentHelper getContentHelper() {
        return this.contentHelper;
    }
}