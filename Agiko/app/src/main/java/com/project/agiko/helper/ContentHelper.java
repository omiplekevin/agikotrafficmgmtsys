package com.project.agiko.helper;

import android.app.Application;

import com.project.agiko.Agiko;
import com.project.agiko.db.DaoSession;
import com.project.agiko.db.Report;
import com.project.agiko.db.ReportDao;

import java.util.List;
import java.util.Vector;

/**
 * Created by OMIPLEKEVIN on 007 May 07, 2017.
 */

public class ContentHelper implements Subject{

    private static volatile ContentHelper instance;

    private List<Report> reportList;
    private Vector<Observer> observers;

    public static ContentHelper getInstance() {
        if (instance == null) {
            if (instance == null) {
                instance = new ContentHelper();
            }
        }
        return instance;
    }

    public ContentHelper() {
        observers = new Vector<>();
    }

    public List<Report> getReportList(Application application) {
        DaoSession daoSession = ((Agiko) application).getDaoSession();
        ReportDao reportDao = daoSession.getReportDao();
        return reportDao.queryBuilder().list();
    }

    public boolean insertContent(Application application, String payload, String description, String owner, String title) {
        try {
            Report report = new Report();
            report.setContent_payload(payload);
            report.setDescription(description);
            report.setOwner(owner);
            report.setTitle(title);
            DaoSession daoSession = ((Agiko) application).getDaoSession();
            ReportDao reportDao = daoSession.getReportDao();
            reportDao.insert(report);
            notifyObservers();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean removeContent(Application application, long contentID) {
        DaoSession daoSession = ((Agiko) application).getDaoSession();
        ReportDao reportDao = daoSession.getReportDao();
        reportDao.deleteByKey(contentID);

        return reportDao.load(contentID) == null;
    }

    @Override
    public void registerObserver(Observer o) {
        if (this.observers != null) {
            this.observers.add(o);
        }
    }

    @Override
    public void removeObserver(Observer o) {
        if (this.observers != null) {
            this.observers.remove(o);
        }
    }

    @Override
    public void notifyObservers() {
        for (Observer observer : observers) {
            observer.update();
        }
    }
}
